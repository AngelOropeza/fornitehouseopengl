﻿#include <iostream>
#include <cmath>

//irrKlang
#include<irrKlang.h>
using namespace irrklang;

// GLEW
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// Other Libs
#include "stb_image.h"

// GLM Mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

//Load Models
#include "SOIL2/SOIL2.h"


// Other includes
#include "Shader.h"
#include "Camera.h"
#include "Model.h"
#include "Texture.h"

// Function prototypes
void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mode);
void MouseCallback(GLFWwindow* window, double xPos, double yPos);
void DoMovement();
void animaciones();

// Window dimensions
const GLuint WIDTH = 1280, HEIGHT = 720;
int SCREEN_WIDTH, SCREEN_HEIGHT;

// Camera
Camera  camera(glm::vec3(0.0f, 3.0f, 0.0f));
GLfloat lastX = WIDTH / 2.0;
GLfloat lastY = HEIGHT / 2.0;
bool keys[1024];
bool firstMouse = true;
float range = 0.0f;


// Light attributes
glm::vec3 lightPos(0.0f, 0.0f, 0.0f);
glm::vec3 PosIni(0.0f, 1.0f, 0.0f);
bool active;


// Deltatime
GLfloat deltaTime = 0.0f;	// Time between current frame and last frame
GLfloat lastFrame = 0.0f;  	// Time of last frame

// Keyframes
float rotDummyLegR = 0, rotDummyLegL = 0;
float posX = PosIni.x, posY = PosIni.y, posZ = PosIni.z;
float rotYarm = 0, rotXarm = 0;

//Orientacion Steve
int reversa = 0;
int frente = 1;
int derecha = 0;
int izquierda = 0;

#define MAX_FRAMES_DUMMY_PIES 4
#define MAX_FRAMES_DUMMY_BRAZO 14


//int i_max_steps = 190;
int i_max_steps = 45; //Hacerlo mas rapido

int i_curr_steps = 0;


typedef struct _frame
{
	float rotDummyLegR;
	float rotIncDummyLegR;

	float rotDummyLegL;
	float rotIncDummyLegL;

}FRAME;

FRAME KeyFrameDummy[MAX_FRAMES_DUMMY_PIES];
int FrameIndexDummy = 0;
bool animaPies = false;
int playIndex = 0;

typedef struct
{
	float rotYarm;
	float rotyIncArm;
	float rotXarm;
	float rotxIncArm;
}FRAME_SALUDO;

FRAME_SALUDO KeyFrameSaludo[MAX_FRAMES_DUMMY_BRAZO];
int FrameIndexSaludo = 0;
bool animaSaludo = false;
int playIndexSaludo = 0;
float steveY = 0;


// Positions of the point lights
glm::vec3 pointLightPositions[] = {
	glm::vec3(posX,posY,posZ),
	glm::vec3(-4.45f,6.73f,-17.45f),
	glm::vec3(1,8.5f,-20),
	glm::vec3(5,8.5,-20)
};

glm::vec3 LightP1;

// ANIMACION PUERTA PRINCIPAL E INTERIOR 
float puerta1rot = 0.0f;
float puerta2rot = 90.0f;
bool dirPuerta, dirPuerta2; //Validacion puerta

// ANIMACIÓN SILLA
float sillaRot = 0.0f;

//ANIMACION CAMIÓN
float movKitX = 0.0;
float movKitZ = 0.0;
float rotKit = 90;
float rotSteve = 0.0;
float xFlag = 0.0;
float zFlag = 0.0;
glm::vec3 PosIniCamion(-15.0f, 30.0f, -30.0f);
bool circuito = false;
bool vuelo1 = true;
bool vuelo2 = false;
bool vuelo3 = false;
bool vuelo4 = false;
bool vuelo5 = false;
int caminata = 0;
bool adelante = true;
bool atras = false;

void saveFrame(void)
{
	KeyFrameDummy[0].rotDummyLegL = 0;
	KeyFrameDummy[1].rotDummyLegL = -60;
	KeyFrameDummy[2].rotDummyLegL = 0;
	KeyFrameDummy[0].rotDummyLegR = 0;
	KeyFrameDummy[1].rotDummyLegR = -55;
	KeyFrameDummy[2].rotDummyLegR = -0;

	FrameIndexDummy = 3;

	KeyFrameSaludo[0].rotXarm = 0;
	KeyFrameSaludo[0].rotYarm = 0;
	KeyFrameSaludo[1].rotXarm = -60;
	KeyFrameSaludo[2].rotXarm = -125;

	KeyFrameSaludo[3].rotYarm = 35;
	KeyFrameSaludo[3].rotXarm = -125;
	KeyFrameSaludo[4].rotYarm = 0;
	KeyFrameSaludo[4].rotXarm = -125;
	KeyFrameSaludo[5].rotYarm = -30;
	KeyFrameSaludo[5].rotXarm = -125;
	KeyFrameSaludo[6].rotYarm = 0;
	KeyFrameSaludo[6].rotXarm = -125;
	KeyFrameSaludo[7].rotYarm = 35;
	KeyFrameSaludo[7].rotXarm = -125;
	KeyFrameSaludo[8].rotYarm = 0;
	KeyFrameSaludo[8].rotXarm = -125;
	KeyFrameSaludo[9].rotYarm = -30;
	KeyFrameSaludo[9].rotXarm = -125;
	KeyFrameSaludo[10].rotYarm = 0;
	KeyFrameSaludo[10].rotXarm = -125;

	KeyFrameSaludo[11].rotXarm = -60;
	KeyFrameSaludo[12].rotXarm = 0;

	FrameIndexSaludo = 13;

}

void resetElements(void)
{
	rotDummyLegL = KeyFrameDummy[0].rotDummyLegL;
	rotDummyLegR = KeyFrameDummy[0].rotDummyLegR;
}

void interpolation(void)
{
	KeyFrameDummy[playIndex].rotIncDummyLegL = (KeyFrameDummy[playIndex + 1].rotDummyLegL - KeyFrameDummy[playIndex].rotDummyLegL) / i_max_steps;
	KeyFrameDummy[playIndex].rotIncDummyLegR = (KeyFrameDummy[playIndex + 1].rotDummyLegR - KeyFrameDummy[playIndex].rotDummyLegR) / i_max_steps;
}

void resetElementsSaludo(void) {
	rotYarm = KeyFrameSaludo[0].rotYarm;
	rotXarm = KeyFrameSaludo[0].rotXarm;
}

void interpolationSaludo(void) {
	KeyFrameSaludo[playIndexSaludo].rotxIncArm = (KeyFrameSaludo[playIndexSaludo + 1].rotXarm - KeyFrameSaludo[playIndexSaludo].rotXarm) / i_max_steps;
	KeyFrameSaludo[playIndexSaludo].rotyIncArm = (KeyFrameSaludo[playIndexSaludo + 1].rotYarm - KeyFrameSaludo[playIndexSaludo].rotYarm) / i_max_steps;
}


int main()
{
	// Init GLFW
	glfwInit();

	// Create a GLFWwindow object that we can use for GLFW's functions
	GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "FORTNITE - PROYECTO FINAL", nullptr, nullptr);

	if (nullptr == window)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();

		return EXIT_FAILURE;
	}

	

	glfwMakeContextCurrent(window);

	glfwGetFramebufferSize(window, &SCREEN_WIDTH, &SCREEN_HEIGHT);

	// Set the required callback functions
	glfwSetKeyCallback(window, KeyCallback);
	glfwSetCursorPosCallback(window, MouseCallback);
	printf("%f", glfwGetTime());
	
	// GLFW Options
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
	glewExperimental = GL_TRUE;
	// Initialize GLEW to setup the OpenGL Function pointers
	if (GLEW_OK != glewInit())
	{
		std::cout << "Failed to initialize GLEW" << std::endl;
		return EXIT_FAILURE;
	}

	// Define the viewport dimensions
	glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

	// OpenGL options
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//SHADERS
	Shader lightingShader("Shaders/lighting.vs", "Shaders/lighting.frag");
	Shader lampShader("Shaders/lamp.vs", "Shaders/lamp.frag");
	Shader SkyBoxshader("Shaders/SkyBox.vs", "Shaders/SkyBox.frag");

	//***************M O D E L O S//******************
	//ESTÁTICOS
	Model Fachada((char*)"Models/objStatics/Fachada/fachada.obj");
	Model Buro((char*)"Models/objStatics/Buro/buro.obj");
	Model Cama((char*)"Models/objStatics/Cama/cama.obj");
	Model Cesto((char*)"Models/objStatics/Cesto/cesto.obj");
	Model Escritorio((char*)"Models/objStatics/Escritorio/escritorio.obj");
	Model Lampara((char*)"Models/objStatics/Lampara/lampara.obj");
	Model Librero((char*)"Models/objStatics/Librero/librero.obj");
	//DINÁMICOS
	Model SillaInferior((char*)"Models/objDynamics/silla/sillaInferior.obj");
	Model SillaSuperior((char*)"Models/objDynamics/silla/sillaSuperior.obj");

	Model Puerta((char*)"Models/objDynamics/door/door.obj");

	Model DummyBody((char*)"Models/objDynamics/dummy/cuerpo.obj");
	Model DummyLegR((char*)"Models/objDynamics/dummy/piernaDer.obj");
	Model DummyLegL((char*)"Models/objDynamics/dummy/piernaIzq.obj");
	Model DummyArmR((char*)"Models/objDynamics/dummy/brazoDer.obj");

	Model Camion((char*)"Models/objDynamics/camion/camion.obj");

	Model ourModelSteve((char*)"Models/objDynamics/Steve/steve.obj");
	Model ourModelPiernaSteve((char*)"Models/objDynamics/Steve/pierna-steve.obj");
	Model ourModelBrazoSteve((char*)"Models/objDynamics/Steve/brazo-steve.obj");

	Model Portal((char*)"Models/objStatics/Portal/portal.obj");

	// Start the sound engine with default parameters
	ISoundEngine* engine = createIrrKlangDevice();

	if (!engine)
		return 0; // error starting up the engine

	  // play some sound stream, looped
	engine->play2D("Sound/lobby.mp3", true);

	// Build and compile our shader program

	//Inicializacion de KeyFrames
	for (int i = 0; i < MAX_FRAMES_DUMMY_PIES; i++) {
		KeyFrameDummy[i].rotDummyLegL = 0;
		KeyFrameDummy[i].rotIncDummyLegL = 0;
		KeyFrameDummy[i].rotDummyLegR = 0;
		KeyFrameDummy[i].rotIncDummyLegR = 0;
	}
	for (int i = 0; i < MAX_FRAMES_DUMMY_BRAZO; i++) {
		KeyFrameSaludo[i].rotXarm = 0;
		KeyFrameSaludo[i].rotxIncArm = 0;
		KeyFrameSaludo[i].rotYarm = 0;
		KeyFrameSaludo[i].rotyIncArm = 0;
	}



	// Set up vertex data (and buffer(s)) and attribute pointers
	GLfloat vertices[] =
	{
		// Positions            // Normals              // Texture Coords
		-0.5f, -0.5f, -0.5f,    0.0f,  0.0f, -1.0f,     0.0f,  0.0f,
		0.5f, -0.5f, -0.5f,     0.0f,  0.0f, -1.0f,     1.0f,  0.0f,
		0.5f,  0.5f, -0.5f,     0.0f,  0.0f, -1.0f,     1.0f,  1.0f,
		0.5f,  0.5f, -0.5f,     0.0f,  0.0f, -1.0f,     1.0f,  1.0f,
		-0.5f,  0.5f, -0.5f,    0.0f,  0.0f, -1.0f,     0.0f,  1.0f,
		-0.5f, -0.5f, -0.5f,    0.0f,  0.0f, -1.0f,     0.0f,  0.0f,

		-0.5f, -0.5f,  0.5f,    0.0f,  0.0f,  1.0f,     0.0f,  0.0f,
		0.5f, -0.5f,  0.5f,     0.0f,  0.0f,  1.0f,     1.0f,  0.0f,
		0.5f,  0.5f,  0.5f,     0.0f,  0.0f,  1.0f,     1.0f,  1.0f,
		0.5f,  0.5f,  0.5f,     0.0f,  0.0f,  1.0f,  	1.0f,  1.0f,
		-0.5f,  0.5f,  0.5f,    0.0f,  0.0f,  1.0f,     0.0f,  1.0f,
		-0.5f, -0.5f,  0.5f,    0.0f,  0.0f,  1.0f,     0.0f,  0.0f,

		-0.5f,  0.5f,  0.5f,    -1.0f,  0.0f,  0.0f,    1.0f,  0.0f,
		-0.5f,  0.5f, -0.5f,    -1.0f,  0.0f,  0.0f,    1.0f,  1.0f,
		-0.5f, -0.5f, -0.5f,    -1.0f,  0.0f,  0.0f,    0.0f,  1.0f,
		-0.5f, -0.5f, -0.5f,    -1.0f,  0.0f,  0.0f,    0.0f,  1.0f,
		-0.5f, -0.5f,  0.5f,    -1.0f,  0.0f,  0.0f,    0.0f,  0.0f,
		-0.5f,  0.5f,  0.5f,    -1.0f,  0.0f,  0.0f,    1.0f,  0.0f,

		0.5f,  0.5f,  0.5f,     1.0f,  0.0f,  0.0f,     1.0f,  0.0f,
		0.5f,  0.5f, -0.5f,     1.0f,  0.0f,  0.0f,     1.0f,  1.0f,
		0.5f, -0.5f, -0.5f,     1.0f,  0.0f,  0.0f,     0.0f,  1.0f,
		0.5f, -0.5f, -0.5f,     1.0f,  0.0f,  0.0f,     0.0f,  1.0f,
		0.5f, -0.5f,  0.5f,     1.0f,  0.0f,  0.0f,     0.0f,  0.0f,
		0.5f,  0.5f,  0.5f,     1.0f,  0.0f,  0.0f,     1.0f,  0.0f,

		-0.5f, -0.5f, -0.5f,    0.0f, -1.0f,  0.0f,     0.0f,  1.0f,
		0.5f, -0.5f, -0.5f,     0.0f, -1.0f,  0.0f,     1.0f,  1.0f,
		0.5f, -0.5f,  0.5f,     0.0f, -1.0f,  0.0f,     1.0f,  0.0f,
		0.5f, -0.5f,  0.5f,     0.0f, -1.0f,  0.0f,     1.0f,  0.0f,
		-0.5f, -0.5f,  0.5f,    0.0f, -1.0f,  0.0f,     0.0f,  0.0f,
		-0.5f, -0.5f, -0.5f,    0.0f, -1.0f,  0.0f,     0.0f,  1.0f,

		-0.5f,  0.5f, -0.5f,    0.0f,  1.0f,  0.0f,     0.0f,  1.0f,
		0.5f,  0.5f, -0.5f,     0.0f,  1.0f,  0.0f,     1.0f,  1.0f,
		0.5f,  0.5f,  0.5f,     0.0f,  1.0f,  0.0f,     1.0f,  0.0f,
		0.5f,  0.5f,  0.5f,     0.0f,  1.0f,  0.0f,     1.0f,  0.0f,
		-0.5f,  0.5f,  0.5f,    0.0f,  1.0f,  0.0f,     0.0f,  0.0f,
		-0.5f,  0.5f, -0.5f,    0.0f,  1.0f,  0.0f,     0.0f,  1.0f
	};


	GLfloat skyboxVertices[] = {
		// Positions
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		-1.0f,  1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,
		
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f,  1.0f
	};


	GLuint indices[] =
	{  // Note that we start from 0!
		0,1,2,3,
		4,5,6,7,
		8,9,10,11,
		12,13,14,15,
		16,17,18,19,
		20,21,22,23,
		24,25,26,27,
		28,29,30,31,
		32,33,34,35
	};

	// Positions all containers
	glm::vec3 cubePositions[] = {
		glm::vec3(0.0f,  0.0f,  0.0f),
		glm::vec3(2.0f,  5.0f, -15.0f),
		glm::vec3(-1.5f, -2.2f, -2.5f),
		glm::vec3(-3.8f, -2.0f, -12.3f),
		glm::vec3(2.4f, -0.4f, -3.5f),
		glm::vec3(-1.7f,  3.0f, -7.5f),
		glm::vec3(1.3f, -2.0f, -2.5f),
		glm::vec3(1.5f,  2.0f, -2.5f),
		glm::vec3(1.5f,  0.2f, -1.5f),
		glm::vec3(-1.3f,  1.0f, -1.5f)
	};


	// First, set the container's VAO (and VBO)
	GLuint VBO, VAO, EBO;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	// Position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);
	// Normals attribute
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
	// Texture Coordinate attribute
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
	glEnableVertexAttribArray(2);
	glBindVertexArray(0);

	// Then, we set the light's VAO (VBO stays the same. After all, the vertices are the same for the light object (also a 3D cube))
	GLuint lightVAO;
	glGenVertexArrays(1, &lightVAO);
	glBindVertexArray(lightVAO);
	// We only need to bind to the VBO (to link it with glVertexAttribPointer), no need to fill it; the VBO's data already contains all we need.
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	// Set the vertex attributes (only position data for the lamp))
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0); // Note that we skip over the other data in our buffer object (we don't need the normals/textures, only positions).
	glEnableVertexAttribArray(0);
	glBindVertexArray(0);


	GLuint skyboxVBO, skyboxVAO;
	glGenVertexArrays(1, &skyboxVAO);
	glGenBuffers(1, &skyboxVBO);
	glBindVertexArray(skyboxVAO);
	glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);

	// Load textures
	vector<const GLchar*> faces;
	faces.push_back("SkyBox/left.tga");
	faces.push_back("SkyBox/right.tga");
	faces.push_back("SkyBox/top.tga");
	faces.push_back("SkyBox/bottom.tga");
	faces.push_back("SkyBox/back.tga");
	faces.push_back("SkyBox/front.tga");

	GLuint cubemapTexture = TextureLoading::LoadCubemap(faces);

	glm::mat4 projection = glm::perspective(camera.GetZoom(), (GLfloat)SCREEN_WIDTH / (GLfloat)SCREEN_HEIGHT, 0.1f, 1000.0f);

	// Game loop
	while (!glfwWindowShouldClose(window))
	{

		// Calculate deltatime of current frame
		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		// Check if any events have been activiated (key pressed, mouse moved etc.) and call corresponding response functions
		glfwPollEvents();
		DoMovement();
		animaciones();


		// Clear the colorbuffer
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


		// Use cooresponding shader when setting uniforms/drawing objects
		lightingShader.Use();
		GLint viewPosLoc = glGetUniformLocation(lightingShader.Program, "viewPos");
		glUniform3f(viewPosLoc, camera.GetPosition().x, camera.GetPosition().y, camera.GetPosition().z);
		// Set material properties
		glUniform1f(glGetUniformLocation(lightingShader.Program, "material.shininess"), 32.0f);
		// == ==========================
		// Here we set all the uniforms for the 5/6 types of lights we have. We have to set them manually and index
		// the proper PointLight struct in the array to set each uniform variable. This can be done more code-friendly
		// by defining light types as classes and set their values in there, or by using a more efficient uniform approach
		// by using 'Uniform buffer objects', but that is something we discuss in the 'Advanced GLSL' tutorial.
		// == ==========================
		// Directional light
		glUniform3f(glGetUniformLocation(lightingShader.Program, "dirLight.direction"), -0.2f, -1.0f, -0.3f);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "dirLight.ambient"), 0.3f, 0.3f, 0.3f);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "dirLight.diffuse"), 0.4f, 0.4f, 0.4f);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "dirLight.specular"), 0.5f, 0.5f, 0.5f);


		// Point light 1
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[0].position"), pointLightPositions[0].x, pointLightPositions[0].y, pointLightPositions[0].z);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[0].ambient"), 0.01f, 0.01f, 0.01f);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[0].diffuse"), LightP1.x, LightP1.y, LightP1.z);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[0].specular"), LightP1.x, LightP1.y, LightP1.z);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[0].constant"), 1.0f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[0].linear"), 0.07f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[0].quadratic"), 0.020f);



		// Point light 2
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[1].position"), pointLightPositions[1].x, pointLightPositions[1].y, pointLightPositions[1].z);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[1].ambient"), 0.03f, 0.03f, 0.03f);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[1].diffuse"), 1.0f, 1.0f, 1.0f);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[1].specular"), 1.0f, 1.0f, 1.0f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[1].constant"), 1.0f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[1].linear"), 0.09f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[1].quadratic"), 0.032f);

		// Point light 3
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[2].position"), pointLightPositions[2].x, pointLightPositions[2].y, pointLightPositions[2].z);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[2].ambient"), 0.03f, 0.03f, 0.03f);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[2].diffuse"), 1.0f, 1.0f, 1.0f);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[2].specular"), 1.0f, 1.0f, 1.0f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[2].constant"), 1.0f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[2].linear"), 0.09f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[2].quadratic"), 0.032f);

		// Point light 4
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[3].position"), pointLightPositions[3].x, pointLightPositions[3].y, pointLightPositions[3].z);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[3].ambient"), 0.03f, 0.03f, 0.03f);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[3].diffuse"), 1.0f, 1.0f, 1.0f);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[3].specular"), 1.0f, 1.0f, 1.0f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[3].constant"), 1.0f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[3].linear"), 0.09f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[3].quadratic"), 0.032f);

		// SpotLight
		glUniform3f(glGetUniformLocation(lightingShader.Program, "spotLight.position"), camera.GetPosition().x, camera.GetPosition().y, camera.GetPosition().z);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "spotLight.direction"), camera.GetFront().x, camera.GetFront().y, camera.GetFront().z);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "spotLight.ambient"), 0.0f, 0.0f, 0.0f);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "spotLight.diffuse"), 0.0f, 0.0f, 0.0f);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "spotLight.specular"), 0.0f, 0.0f, 0.0f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "spotLight.constant"), 1.0f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "spotLight.linear"), 0.09f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "spotLight.quadratic"), 0.032f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "spotLight.cutOff"), glm::cos(glm::radians(12.5f)));
		glUniform1f(glGetUniformLocation(lightingShader.Program, "spotLight.outerCutOff"), glm::cos(glm::radians(15.0f)));

		// Set material properties
		glUniform1f(glGetUniformLocation(lightingShader.Program, "material.shininess"), 32.0f);

		// Create camera transformations
		glm::mat4 view;
		view = camera.GetViewMatrix();


		// Get the uniform locations
		GLint modelLoc = glGetUniformLocation(lightingShader.Program, "model");
		GLint viewLoc = glGetUniformLocation(lightingShader.Program, "view");
		GLint projLoc = glGetUniformLocation(lightingShader.Program, "projection");

		// Pass the matrices to the shader
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));


		glBindVertexArray(VAO);
		glm::mat4 tmp = glm::mat4(1.0f); //Temp
		glm::mat4 modelTemp = glm::mat4(1.0f);
		glm::mat4 modelTemp2 = glm::mat4(1.0f);


		//***************C A R G A  M O D E L O S***************
		//FACHADA - REFERENCIA
		view = camera.GetViewMatrix();
		glm::mat4 model(1);
		tmp = model = glm::translate(model, glm::vec3(1.9f, 2.4f, -20.0f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		Fachada.Draw(lightingShader);

		//BURO
		view = camera.GetViewMatrix();
		model = glm::mat4(1);
		model = glm::translate(tmp, glm::vec3(-3.8f, 2.68f, 0.1f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		Buro.Draw(lightingShader);

		//CAMA
		view = camera.GetViewMatrix();
		model = glm::mat4(1);
		model = glm::translate(tmp, glm::vec3(-4.55f, 2.66f, 1.5f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		Cama.Draw(lightingShader);

		//CESTO
		view = camera.GetViewMatrix();
		model = glm::mat4(1);
		model = glm::translate(tmp, glm::vec3(-3.8f, 2.68f, -3.3f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		Cesto.Draw(lightingShader);

		//ESCRITORIO
		view = camera.GetViewMatrix();
		model = glm::mat4(1);
		model = glm::translate(tmp, glm::vec3(-5.5f, 2.66f, -3.3f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		Escritorio.Draw(lightingShader);

		//LAMPARA
		view = camera.GetViewMatrix();
		model = glm::mat4(1);
		model = glm::translate(tmp, glm::vec3(-6.4f, 2.62f, 2.7f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		Lampara.Draw(lightingShader);

		//LIBRERO
		view = camera.GetViewMatrix();
		model = glm::mat4(1);
		model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::translate(tmp, glm::vec3(-6.66f, 2.62f, 0.2f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		Librero.Draw(lightingShader);

		//**DINÁMICOS**
		//SILLA
		view = camera.GetViewMatrix();
		model = glm::mat4(1);
		model = glm::translate(tmp, glm::vec3(-6.0f, 2.67f, -2.5f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		SillaInferior.Draw(lightingShader);
		view = camera.GetViewMatrix();
		model = glm::mat4(1);
		model = glm::translate(tmp, glm::vec3(-6.0f, 2.65f, -2.5f));
		model = glm::rotate(model, glm::radians(sillaRot), glm::vec3(0.0f, 1.0f, 0.0f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		SillaSuperior.Draw(lightingShader);

		//PUERTA PRINCIPAL
		view = camera.GetViewMatrix();
		model = glm::mat4(1);
		model = glm::translate(tmp, glm::vec3(-2.08f, -0.03f, 6.82f));
		model = glm::rotate(model, glm::radians(puerta1rot), glm::vec3(0.0f, 1.0f, 0.0f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		Puerta.Draw(lightingShader);

		//PUERTA SECUNDARIA
		view = camera.GetViewMatrix();
		model = glm::mat4(1);
		model = glm::translate(tmp, glm::vec3(-3.45f, 2.67f, -1.56f));
		model = glm::rotate(model, glm::radians(puerta2rot), glm::vec3(0.0f, 1.0f, 0.0f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		Puerta.Draw(lightingShader);

		//CAMION
		view = camera.GetViewMatrix();
		model = glm::mat4(1);
		model = glm::translate(model, PosIniCamion + glm::vec3(movKitX, 0, movKitZ));
		model = glm::rotate(model, glm::radians(rotKit), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		Camion.Draw(lightingShader);

		//DUMMY
		//cuerpo
		view = camera.GetViewMatrix();
		model = glm::mat4(1);
		model = glm::translate(tmp, glm::vec3(-5.0f, 0.0f, 5.5f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		DummyBody.Draw(lightingShader);
		//piernaIzquierda
		view = camera.GetViewMatrix();
		model = glm::mat4(1);
		model = glm::translate(tmp, glm::vec3(-5.125f, 0.70f, 6.55f));
		model = glm::rotate(model, glm::radians(rotDummyLegR), glm::vec3(1.0f, 0.0f, 0.0f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		DummyLegL.Draw(lightingShader);
		//piernaDerecha
		view = camera.GetViewMatrix();
		model = glm::mat4(1);
		model = glm::translate(tmp, glm::vec3(-4.885f, 0.70f, 6.55f));
		model = glm::rotate(model, glm::radians(rotDummyLegL), glm::vec3(1.0f, 0.0f, 0.0f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		DummyLegR.Draw(lightingShader);
		//brazoDerecho
		view = camera.GetViewMatrix();
		model = glm::mat4(1);
		modelTemp = model = glm::translate(tmp, glm::vec3(-4.82f, 1.19f, 6.2f));
		model = glm::rotate(model, glm::radians(rotXarm), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::rotate(model, glm::radians(rotYarm), glm::vec3(0.0f, 1.0f, 0.0f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		DummyArmR.Draw(lightingShader);

		//STEVE
		view = camera.GetViewMatrix();
		model = glm::mat4(1);
		modelTemp = glm::mat4(1);
		modelTemp = model = glm::translate(model, glm::vec3(camera.GetPosition().x , camera.GetPosition().y+0.3f+steveY, camera.GetPosition().z-2.0f));
		if (reversa == 0 && frente == 1 && izquierda == 0 && derecha == 0) { //FRENTE
			model = glm::rotate(model, glm::radians(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		}
		else if (izquierda == 1 && derecha == 0 && reversa == 0 && frente == 0) { /*IZQUIERDA*/
			model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		}
		else if (izquierda == 0 && derecha == 1 && reversa == 0 && frente == 0) { /*DERECHA*/
			model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		}
		else if (reversa == 1 && frente == 0 && izquierda == 0 && derecha == 0) { /*ATRAS*/
			model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		}
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		ourModelSteve.Draw(lightingShader);
		glBindVertexArray(0);

		//Brazo derecho Steve 
		view = camera.GetViewMatrix();
		model = glm::mat4(1);
		if (reversa == 0 && frente == 1 && izquierda == 0 && derecha == 0) { //FRENTE
			model = glm::translate(modelTemp, glm::vec3(0.25f, 0.055f, 0.0f));
			model = glm::rotate(model, glm::radians(rotSteve), glm::vec3(1.0f, 0.0f, 0.0f));
		}
		else if (izquierda == 1 && derecha == 0 && reversa == 0 && frente == 0) { /*IZQUIERDA*/
			model = glm::translate(modelTemp, glm::vec3(0.0f, 0.055f, 0.25f));
			model = glm::rotate(model, glm::radians(rotSteve), glm::vec3(0.0f, 0.0f, 1.0f));

		}
		else if (izquierda == 0 && derecha == 1 && reversa == 0 && frente == 0) { /*DERECHA*/
			model = glm::translate(modelTemp, glm::vec3(0.0f, 0.055f, 0.25f));
			model = glm::rotate(model, glm::radians(rotSteve), glm::vec3(0.0f, 0.0f, 1.0f));
		}
		else if (reversa == 1 && frente == 0 && izquierda == 0 && derecha == 0) { /*ATRAS*/
			model = glm::translate(modelTemp, glm::vec3(0.25f, 0.055f, 0.0f));
			model = glm::rotate(model, glm::radians(rotSteve), glm::vec3(1.0f, 0.0f, 0.0f));
		}
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		ourModelBrazoSteve.Draw(lightingShader);

		//Brazo izquierdo Steve 
		view = camera.GetViewMatrix();
		model = glm::mat4(1);
		if (reversa == 0 && frente == 1 && izquierda == 0 && derecha == 0) { //FRENTE
			model = glm::translate(modelTemp, glm::vec3(-0.25f, 0.055f, 0.0f));
			model = glm::rotate(model, glm::radians(rotSteve * -1.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		}
		else if (izquierda == 1 && derecha == 0 && reversa == 0 && frente == 0) { /*IZQUIERDA*/
			model = glm::translate(modelTemp, glm::vec3(0.0f, 0.055f, -0.25f));
			model = glm::rotate(model, glm::radians(rotSteve * -1.0f), glm::vec3(0.0f, 0.0f, 1.0f));
		}
		else if (izquierda == 0 && derecha == 1 && reversa == 0 && frente == 0) { /*DERECHA*/
			model = glm::translate(modelTemp, glm::vec3(0.0f, 0.055f, -0.25f));
			model = glm::rotate(model, glm::radians(rotSteve * -1.0f), glm::vec3(0.0f, 0.0f, 1.0f));
		}
		else if (reversa == 1 && frente == 0 && izquierda == 0 && derecha == 0) { /*ATRAS*/
			model = glm::translate(modelTemp, glm::vec3(-0.25f, 0.055f, 0.0f));
			model = glm::rotate(model, glm::radians(rotSteve * -1.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		}
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		ourModelBrazoSteve.Draw(lightingShader);

		//Pierna derecha Steve 
		view = camera.GetViewMatrix();
		model = glm::mat4(1);
		if (reversa == 0 && frente == 1 && izquierda == 0 && derecha == 0) { //FRENTE
			model = glm::translate(modelTemp, glm::vec3(0.085f, -0.51f, -0.02f));
			model = glm::rotate(model, glm::radians(rotSteve * -1.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		}
		else if (izquierda == 1 && derecha == 0 && reversa == 0 && frente == 0) { /*IZQUIERDA*/
			model = glm::translate(modelTemp, glm::vec3(0.02f, -0.51f, 0.085f));
			model = glm::rotate(model, glm::radians(rotSteve * -1.0f), glm::vec3(0.0f, 0.0f, 1.0f));
		}
		else if (izquierda == 0 && derecha == 1 && reversa == 0 && frente == 0) { /*DERECHA*/
			model = glm::translate(modelTemp, glm::vec3(-0.02f, -0.51f, 0.085f));
			model = glm::rotate(model, glm::radians(rotSteve * -1.0f), glm::vec3(0.0f, 0.0f, 1.0f));
		}
		else if (reversa == 1 && frente == 0 && izquierda == 0 && derecha == 0) { /*ATRAS*/
			model = glm::translate(modelTemp, glm::vec3(0.085f, -0.51f, -0.02f));
			model = glm::rotate(model, glm::radians(rotSteve * -1.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		}
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		ourModelPiernaSteve.Draw(lightingShader);

		//Pierna izquierda Steve 
		view = camera.GetViewMatrix();
		model = glm::mat4(1);
		if (reversa == 0 && frente == 1 && izquierda == 0 && derecha == 0) { //FRENTE
			model = glm::translate(modelTemp, glm::vec3(-0.085f, -0.51f, -0.02f));
			model = glm::rotate(model, glm::radians(rotSteve), glm::vec3(1.0f, 0.0f, 0.0f));
		}
		else if (izquierda == 1 && derecha == 0 && reversa == 0 && frente == 0) { /*IZQUIERDA*/
			model = glm::translate(modelTemp, glm::vec3(0.02f, -0.51f, -0.085f));
			model = glm::rotate(model, glm::radians(rotSteve), glm::vec3(0.0f, 0.0f, 1.0f));
		}
		else if (izquierda == 0 && derecha == 1 && reversa == 0 && frente == 0) { /*DERECHA*/
			model = glm::translate(modelTemp, glm::vec3(-0.02f, -0.51f, -0.085f));
			model = glm::rotate(model, glm::radians(rotSteve), glm::vec3(0.0f, 0.0f, 1.0f));
		}
		else if (reversa == 1 && frente == 0 && izquierda == 0 && derecha == 0) { /*ATRAS*/
			model = glm::translate(modelTemp, glm::vec3(-0.085f, -0.51f, -0.02f));
			model = glm::rotate(model, glm::radians(rotSteve), glm::vec3(1.0f, 0.0f, 0.0f));
		}
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		ourModelPiernaSteve.Draw(lightingShader);

		view = camera.GetViewMatrix();
		model = glm::mat4(1);
		model = glm::translate(tmp, glm::vec3(5.0f, 1.2f, -2.5f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		Portal.Draw(lightingShader);

		// Also draw the lamp object, again binding the appropriate shader
		lampShader.Use();
		// Get location objects for the matrices on the lamp shader (these could be different on a different shader)
		modelLoc = glGetUniformLocation(lampShader.Program, "model");
		viewLoc = glGetUniformLocation(lampShader.Program, "view");
		projLoc = glGetUniformLocation(lampShader.Program, "projection");

		// Set matrices
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
		model = glm::mat4(1);
		model = glm::translate(model, lightPos);
		//model = glm::scale(model, glm::vec3(0.2f)); // Make it a smaller cube
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

		// Draw the light object (using light's vertex attributes)
		glBindVertexArray(lightVAO);
		
		glBindVertexArray(0);


		// Draw skybox as last
		glDepthFunc(GL_LEQUAL);  // Change depth function so depth test passes when values are equal to depth buffer's content
		SkyBoxshader.Use();
		view = glm::mat4(glm::mat3(camera.GetViewMatrix()));	// Remove any translation component of the view matrix
		glUniformMatrix4fv(glGetUniformLocation(SkyBoxshader.Program, "view"), 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(glGetUniformLocation(SkyBoxshader.Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

		// skybox cube
		glBindVertexArray(skyboxVAO);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTexture);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		glBindVertexArray(0);
		glDepthFunc(GL_LESS); // Set depth function back to default


		// Swap the screen buffers
		glfwSwapBuffers(window);


		saveFrame();
	}




	glDeleteVertexArrays(1, &VAO);
	glDeleteVertexArrays(1, &lightVAO);
	glDeleteBuffers(1, &VBO);
	glDeleteBuffers(1, &EBO);
	glDeleteVertexArrays(1, &skyboxVAO);
	glDeleteBuffers(1, &skyboxVBO);
	// Terminate GLFW, clearing any resources allocated by GLFW.
	glfwTerminate();




	return 0;
}

//Declaración de animaciones
void animaciones()
{
	if (animaPies) {
		if (i_curr_steps >= i_max_steps) {
			playIndex++;
			if (playIndex > MAX_FRAMES_DUMMY_PIES - 2) {
				playIndex = 0;
				animaPies = false;
			}
			else {
				i_curr_steps = 0;
				interpolation();
			}
		}
		else {
			rotDummyLegL += KeyFrameDummy[playIndex].rotIncDummyLegL;
			rotDummyLegR += KeyFrameDummy[playIndex].rotIncDummyLegR;

			i_curr_steps++;
		}
	}

	if (animaSaludo) {
		if (i_curr_steps >= i_max_steps) {
			playIndexSaludo++;
			if (playIndexSaludo > MAX_FRAMES_DUMMY_BRAZO - 2) {
				playIndexSaludo = 0;
				animaSaludo = false;
			}
			else {
				i_curr_steps = 0;
				interpolationSaludo();
			}
		}
		else {
			rotXarm += KeyFrameSaludo[playIndexSaludo].rotxIncArm;
			rotYarm += KeyFrameSaludo[playIndexSaludo].rotyIncArm;

			i_curr_steps++;
		}
	}

	if (circuito)
	{
		if (vuelo1)
		{
			movKitX += 1.0f;
			if (movKitX > 25)
			{
				vuelo1 = false;
				vuelo2 = true;

			}
		}
		if (vuelo2)
		{
			rotKit = 0;
			movKitZ += 1.0f;
			if (movKitZ > 25)
			{
				vuelo2 = false;
				vuelo3 = true;
			}
		}

		if (vuelo3)
		{
			rotKit = -90;
			movKitX -= 1.0f;
			if (movKitX < 0)
			{
				vuelo3 = false;
				vuelo4 = true;
			}
		}

		if (vuelo4)
		{
			rotKit = -180;
			movKitZ -= 1.0f;
			if (movKitZ < 0)
			{
				vuelo4 = false;
				vuelo5 = true;
			}
		}
		if (vuelo5)
		{
			rotKit = 90;
			movKitZ += 1.0f;
			if (movKitZ > 0)
			{
				vuelo5 = false;
				vuelo1 = true;
			}
		}


	}

	if (caminata == 1)
	{
		if (adelante)
		{
			rotSteve += 6.0f;
			if (rotSteve > 72.0f)
			{
				adelante = false;
				atras = true;

			}
		}
		if (atras)
		{
			rotSteve -= 6.0f;
			if (rotSteve < -72.0f)
			{
				atras = false;
				adelante = true;
			}
		}
	}
}


// Is called whenever a key is pressed/released via GLFW
void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
{

	if (GLFW_KEY_ESCAPE == key && GLFW_PRESS == action)
	{
		glfwSetWindowShouldClose(window, GL_TRUE);
	}

	if (key >= 0 && key < 1024)
	{
		if (action == GLFW_PRESS)
		{
			keys[key] = true;
		}
		else if (action == GLFW_RELEASE)
		{
			keys[key] = false;
		}
	}

	if (keys[GLFW_KEY_SPACE])
	{
		active = !active;
		if (active)
			LightP1 = glm::vec3(1.0f, 0.0f, 0.0f);
		else
			LightP1 = glm::vec3(0.0f, 0.0f, 0.0f);
	}
}

void MouseCallback(GLFWwindow* window, double xPos, double yPos)
{

	if (firstMouse)
	{
		lastX = xPos;
		lastY = yPos;
		firstMouse = false;
	}

	GLfloat xOffset = xPos - lastX;
	GLfloat yOffset = lastY - yPos;  // Reversed since y-coordinates go from bottom to left

	lastX = xPos;
	lastY = yPos;

	camera.ProcessMouseMovement(xOffset, yOffset);
}

// Moves/alters the camera positions based on user input
void DoMovement()
{
	//MOVIMIENTO DE SILLA
	if (keys[GLFW_KEY_5]) {
		sillaRot += 3.0f;
	}
	if (keys[GLFW_KEY_6]) {
		sillaRot -= 3.0f;
	}
	//MOVIMIENTO PUERTA PRINCIPAL
	if (keys[GLFW_KEY_1]) {
		if (puerta1rot >= -117.0f && puerta1rot <= 117.0f) {
			puerta1rot += 3.0f;
			dirPuerta = true;
		}
		else if (dirPuerta) {
			puerta1rot = 111.0f;
		}
	}
	if (keys[GLFW_KEY_2]) {
		if (puerta1rot >= -117.0f && puerta1rot <= 117.0f) {
			puerta1rot -= 3.0f;
			dirPuerta = false;
		}
		else if (!dirPuerta) {
			puerta1rot = -111.0f;
		}
	}
	//MOVIMIENTO PUERTA INTERIOR
	if (keys[GLFW_KEY_3]) {
		if (puerta2rot >= -27.0f && puerta2rot <= 207.0f) {
			puerta2rot += 3.0f;
			dirPuerta2 = true;
		}
		else if (dirPuerta2) {
			puerta2rot = 201.0f;
		}
	}
	if (keys[GLFW_KEY_4]) {
		if (puerta2rot >= -27.0f && puerta2rot <= 207.0f) {
			puerta2rot -= 3.0f;
			dirPuerta2 = false;
		}
		else if (!dirPuerta2) {
			puerta2rot = -21.0f;
		}
	}
	if (keys[GLFW_KEY_P]) {
		if (animaPies == false && (FrameIndexDummy > 1)) {
			resetElements();
			interpolation();
			animaPies = true;
			playIndex = 0;
			i_curr_steps = 0;
		}
	}
	if (keys[GLFW_KEY_O]) {
		if (animaSaludo == false && (FrameIndexSaludo > 1)) {
			resetElementsSaludo();
			interpolationSaludo();
			animaSaludo = true;
			playIndexSaludo = 0;
			i_curr_steps = 0;
		}
	}
	if (keys[GLFW_KEY_C])
	{
		if (!circuito) {
			circuito = true;
		}
		else
		{
			circuito = false;
		}
	}
	// Camera controls
	if (keys[GLFW_KEY_W] || keys[GLFW_KEY_UP])
	{
		camera.ProcessKeyboard(FORWARD, deltaTime);
		reversa = 0;
		frente = 1;
		izquierda = 0;
		derecha = 0;
		caminata = 1;
	}

	if (keys[GLFW_KEY_S] || keys[GLFW_KEY_DOWN])
	{
		camera.ProcessKeyboard(BACKWARD, deltaTime);
		reversa = 1;
		frente = 0;
		izquierda = 0;
		derecha = 0;
		caminata = 1;

	}

	if (keys[GLFW_KEY_A] || keys[GLFW_KEY_LEFT])
	{
		camera.ProcessKeyboard(LEFT, deltaTime);
		izquierda = 1;
		derecha = 0;
		reversa = 0;
		frente = 0;
		caminata = 1;

	}

	if (keys[GLFW_KEY_D] || keys[GLFW_KEY_RIGHT])
	{
		camera.ProcessKeyboard(RIGHT, deltaTime);
		izquierda = 0;
		derecha = 1;
		reversa = 0;
		frente = 0;
		caminata = 1;

	}
	//Steve vuela 
	if (keys[GLFW_KEY_Q])
	{
		steveY += 0.1f;
	}

	//Steve vuela 
	if (keys[GLFW_KEY_E])
	{
		steveY -= 0.1f;
	}

	if ((keys[GLFW_KEY_A] == false) && (keys[GLFW_KEY_W] == false) && (keys[GLFW_KEY_S] == false) && (keys[GLFW_KEY_D] == false)) {
		caminata = 0;
		rotSteve = 0.0f;
	}
}